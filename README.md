# GOLUM

GOLUM: Gravitational-wave analysis Of Lensed and Unlensed waveform Models. 

The GOLUM methodolgy is a way to get fast and precise parameter estimation for strongly-lensed gravitational wave. For more details, see the [method paper](https://arxiv.org/pdf/2105.04536.pdf)

This Git repo contains the `golum` package, which can do the fast PE as well as reweighting using lensing statistics models, such as [More and More]() and [Haris et al](). 

In addition, there is also the `smeagol` package, which is complementary to the `golum` packages. It makes the link between golum and [hanabi](https://github.com/ricokaloklo/hanabi) in order to apply the selection effects to Golum output using hanabi. 

Golum can be installed indepedendently of smeagol as the requirements for golum are lighter. However, to use smeagol, golum is also needed. 

#### Git structure
The git contains 3 main folders:
- `golum`: contains the golum package and its `setup.py` file
- `smeagol`: contains the smeagol package and its `setup.py` file
- `examples`: folder with typical example of how to use `golum` and `smeagol`.

#### Requirements

The requirements for `golum` are:
- scipy
- bilby (>=1.0.0)
- numpy
- lalinference

The requirements for `smeagol` are:
- golum
- hanabi (>= 0.0.4)
Optionally, for selection effects, one can rely on JAX, [`figaro`](https://github.com/sterinaldi/figaro) or [`denamrf`](https://github.com/ricokaloklo/denmarf) to have a more rapid computation when using lensing statistic catalogs.

### If you use this code

If you use this code, please cite our method papers as 
```
@article{Janquart:2021qov,
    author = "Janquart, Justin and Hannuksela, Otto A. and K., Haris and Van Den Broeck, Chris",
    title = "{A fast and precise methodology to search for and analyse strongly lensed gravitational-wave events}",
    eprint = "2105.04536",
    archivePrefix = "arXiv",
    primaryClass = "gr-qc",
    doi = "10.1093/mnras/stab1991",
    journal = "Mon. Not. Roy. Astron. Soc.",
    volume = "506",
    number = "4",
    pages = "5430--5438",
    year = "2021"
}

@article{Janquart:2023osz,
    author = "Janquart, Justin and Haris, K. and Hannuksela, Otto A. and Van Den Broeck, Chris",
    title = "{The Return of GOLUM: Improving Distributed Joint Parameter Estimation for Strongly-Lensed Gravitational Waves}",
    eprint = "2304.12148",
    archivePrefix = "arXiv",
    primaryClass = "gr-qc",
    month = "4",
    year = "2023"
}
```

### For contributing
If you would like to contribute to this code, feel free to 
do a merge request with the `development` branch as soon as you think your code is functional. You will be added to the listof contributors.

#### Additional info

In case you need more information, feel free to contact me at j.janquart@uu.nl or on Mattermost.
